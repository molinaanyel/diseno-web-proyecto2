
var users = [];


function getUsers(){
   users =localStorage.getItem('usuarios')==null?[]:JSON.parse(localStorage.getItem('usuarios'))
   return users;

}



function validarExistencia(user){
    console.log(user.User);
    let users= getUsers();
    console.log(users.length);
    if(users.length >0){
        for(let i = 0; i < users.length; i ++){
            if(users[i].User == user.User && users[i].Password == user.Password){
                return true;
            }
                
    
        }
    }
    return false;   
}


function userRegistrado(){
    const user = {
        Firstname: document.getElementById('firstname').value,
        Lastname: document.getElementById('lastname').value,
        Fullname :document.getElementById('firstname').value +" "+document.getElementById('lastname').value,
        Speed: '',
        About: '',
        Phone: document.getElementById('phone').value,
        User: document.getElementById('username').value,
        Password: document.getElementById('password').value,
        Repassword: document.getElementById('repassword').value,

    }
    return user;

}


function newUser(){
    const user = {
        User: document.getElementById('user-login').value,
        Password: document.getElementById('password-login').value,

    }
    return user;

}



function addSession(){
    let user = newUser();
    console.log(user.User);
    const session = {
        User: user.User,
        Estado: true,

    }
    localStorage.setItem('sesion', JSON.stringify(session));
}


function addUser(){
    let users = getUsers();
    let user = userRegistrado();
    users.push(user);
    localStorage.setItem('usuarios',JSON.stringify(users));
}





function validarForm(){
   
    
    let us = userRegistrado();
    let val = false;
    if( us.Password.length <8){
        val= true;
        console.log("largo incorrecto")
        document.getElementById('alert-registrer').style.display='block';
        document.getElementById('alert-registrer').innerHTML='largo incorrecto'

    }
    if(us.Password !== us.Repassword){
        val= true;
        console.log("No coinciden")
        document.getElementById('alert-registrer').style.display='block';
        document.getElementById('alert-registrer').innerHTML='No coinciden'

    }

    if(us.Password === 0){
        val= true;
        console.log("Introduzca la contraseña")
        document.getElementById('alert-registrer').style.display='block';
        document.getElementById('alert-registrer').innerHTML='Introduzca la contraseña'

    }
    if(us.Firstname === ''){
        val= true;
        console.log("nombr incorrecto")
    }
    if(us.User === ''){
        val= true;
        console.log("user incorrecto")
    }
    if(validarExistencia(us)){
        val= true;

    }
    return val;
}




function validarLogin(){
    const logeado = newUser();
    if(logeado.User === ''){
       return false;
    }

    if(logeado.Password === ''){
        return false;
    }
    if(!validarExistencia(logeado)){
       return false;
        

    }

    return true;
 
}




function cerrarSesion() {
    localStorage.removeItem('sesion');
    window.open("../index.html");
  }
  

  function userLogeado(){
    let sesion = JSON.parse(localStorage.getItem('sesion'));
    let logeado = sesion.User;
    return logeado;

  }

function consultarSession(id){
    let sesion = JSON.parse(localStorage.getItem('sesion'));
      if (sesion.Estado == true) {
        var username = sesion.User;
        document.getElementById(id).innerHTML= username;
      }

}


function buttonLogin(){
   
    console.log(validarLogin());
    if(validarLogin()){
        console.log(addSession());
        addSession();
        window.open('dashboard.html');
        
    }else{
        document.getElementById('alert-login').style.display='block';
      
    }
}

console.log(localStorage.getItem('usuarios'))


console.log(localStorage.getItem('sesion'));

//console.log(localStorage.getItem('rides'));
console.log(localStorage.getItem('rideactual'));



function buttonRegistrer(){
    console.log(validarForm());
    if(!validarForm()){
        addUser();
        limpiarCampos('form-control');
    }
    

}

function limpiarCampos(clase){
    let inputs = document.getElementsByClassName(clase);
    for(let i=0; i < inputs.length;i++){
        inputs[i].value= "";
    }
}



function newRide(id,name,start,end,description,departure,arrival){
    const newride = {
        Id: id,
        User: userLogeado(),
        Name: document.getElementById(name).value,
        Start: document.getElementById(start).value,
        End: document.getElementById(end).value,
        Description: document.getElementById(description).value,
        Departure: document.getElementById(departure).value,
        Arrival: document.getElementById(arrival).value,
        Days: listDays(),

    }
    return newride;
}
    
   
function generarId(){
    let id =  uuid.v4();
    return id;
}



function validarRide(id,name,start,end,description,departure,arrival){
    const ride = newRide(id,name,start,end,description,departure,arrival);
    let flag = true;
    if(ride.Name === ''){
        flag = false;

    }
    if(ride.Start === ''){
        flag = false
    }

    if(ride.Description === ''){
        flag = false
    }
    if(ride.End === ''){
        flag = false

    }
    if(ride.Departure === ''){
        flag = false

    }
    if(ride.Arrival === ''){
        flag = false

    }
    if(!ride.Days.length){
        flag = false

    }

    return flag;


}



function getRides(){
    let rides=[];
    rides =localStorage.getItem('rides')==null?[]:JSON.parse(localStorage.getItem('rides'))  
    return rides;
 
 }

 function btnAddRide(){
     let id = generarId();
     let name = 'ridename-add';
     let start= 'startfrom-add';
     let end = 'end-add';
     let description= 'description-add';
     let departure = 'departure-add';
     let arrival= 'arrival-add';
     if(validarRide(id,name,start,end,description,departure,arrival)){
         addRide(id,name,start,end,description,departure,arrival);
         limpiarCampos('form-control');
         cleanDays();
     }else{
        console.log("no se logro registrar");
     }
 }



 function cleanDays(){
    let dias =document.getElementsByClassName('form-check-input');
    for(let i=0; i < dias.length;i++){
        dias[i].checked= false;

       
     }
}





function listDays(){
    let lista = [];
    let dias =document.getElementsByClassName('form-check-input');
    for(let i=0; i < dias.length;i++){
        if(dias[i].checked){
            let d =dias[i].value;
            lista.push(d);          
        }
     }
     
    return lista;
}

function ridesByUser(){
    let rides = getRides();
    const user= userLogeado();
    let ridesuser= rides.filter(r => r.User === user);
    return ridesuser;
    
}



function addRide(id,name,start,end,description,departure,arrival){
    let listarides = getRides();
    let ride =newRide(id,name,start,end,description,departure,arrival);
    listarides.push(ride);
    localStorage.setItem('rides',JSON.stringify(listarides));
}
    



function addRowRide(){
    let rides= ridesByUser();
    console.log(rides);
    let table = document.getElementById('tbody');
    if(!!rides.length){
        for(let i = 0; i < rides.length; i++){
                table.innerHTML+=`
                <tr>
                <td>${rides[i].Name}</td> 
                <td>${rides[i].Start}</td>
                <td>${rides[i].End}</td>
                <td> <button type='button' class='btn btn-default mr-2' onclick="sendRide('${rides[i].Id}');" style ='font-size: 1em'> 
                <i class='bi bi-pencil-square text-primary'></i>
                <button type='button' class='btn btn-default mr-2'  onclick="deleteRide('${rides[i].Id}');" style ='font-size: 1em'>
                <i class='bi bi-x-square text-danger'></i>
                <button type='button' class='btn btn-default mr-2' onclick="watchRide('${rides[i].Id}');" style ='font-size: 1em'> 
                <i class='bi bi-eye text-primary'></i>
                </td>
                </tr>
                `;
    
            }
           

    }
   
}

function loadSettings(){
   consultarSession('sett-user');
   let sesion = userLogeado();
   let users = getUsers();
   for(let i = 0; i < users.length; i++ ){
       if(users[i].User == sesion){
         document.getElementById('fullname').value= users[i].Fullname;
         document.getElementById('speed').value= users[i].Speed;
         document.getElementById('aboutme').value= users[i].About;

           
       }
   }

}

function editSettings(){
    let sesion = userLogeado();
    let users = getUsers();
    for(let i = 0; i < users.length; i++ ){
        if(users[i].User == sesion){
           users[i].Fullname= document.getElementById('fullname').value;
           users[i].Speed= document.getElementById('speed').value;
           users[i].About=  document.getElementById('aboutme').value;
 
            
        }
    }
    localStorage.setItem('usuarios',JSON.stringify(users));
 
 }


 function filtroRides(){
     let rides = getRides();
     let from = document.getElementById('from-index').value;
     let to = document.getElementById('to-index').value;
     let resultados = rides.filter(r => r.Start.toLowerCase() === from.toLowerCase() && r.End.toLowerCase() === to.toLowerCase());
     return resultados;
 }


f



function loadFiltroR(){
    let rides = filtroRides();
    console.log(rides);
    let table = document.getElementById('index-tbody');
    if(!!rides.length){
        for(let i = 0; i < rides.length; i++){
            table.innerHTML+=`
                <tr>
                <td>${rides[i].User}</td> 
                <td>${rides[i].Start}</td>
                <td>${rides[i].End}</td>
                </tr>
                `;
            
         
               
    
        }

        limpiarCampos('inputs-index');
           
    }
   
}




function cargarRides(){
    consultarSession('das-user');
    addRowRide();

}


function watchRide(id){
    localStorage.setItem('rideactual', JSON.stringify(id));
    window.open("verRide.html")
}



function sendRide(id){
    localStorage.setItem('rideactual', JSON.stringify(id));
    window.open("editarRide.html")
}

function loadEdit(){
    loadRide('ridename-edit','startfrom-edit','end-edit','description-edit','departure-edit','arrival-edit');
    consultarSession('ed-user');
}

function loadAddRide(){
    loadRide('ridename-watch','startfrom-watch','end-watch','description-watch','departure-watch','arrival-watch');
    consultarSession('w-user');

}



function loadRide(name,start,end,descr,dep,arr){ 
    let ride = JSON.parse(localStorage.getItem('rideactual'));
    let rides = getRides();
    let nueva = rides.filter(r => r.Id === ride);
    let days= [];
    for(i = 0; i < nueva.length; i++){
        document.getElementById(name).value= nueva[i].Name;
        document.getElementById(start).value= nueva[i].Start;
        document.getElementById(end).value= nueva[i].End;
        document.getElementById(descr).value= nueva[i].Description;
        document.getElementById(dep).value= nueva[i].Departure;
        document.getElementById(arr).value= nueva[i].Arrival;
        days = nueva[i].Days;
    }
    loadDays(days);
    
}





function btnEditRide(){
     let id =JSON.parse(localStorage.getItem('rideactual'));
     let name = 'ridename-edit';
     let start= 'startfrom-edit';
     let end = 'end-edit';
     let description= 'description-edit';
     let departure = 'departure-edit';
     let arrival= 'arrival-edit';
     if(validarRide(id,name,start,end,description,departure,arrival)){
         editRide(id,name,start,end,description,departure,arrival);
         console.log("agregado con exito")
     }else{
        console.log("no se logro registrar");
     }
}


function editRide(id,name,start,end,description,departure,arrival){   
    let rides = getRides();
    for(let i = 0; i < rides.length; i++){
        if(rides[i].Id === id){
            rides[i]= newRide(id,name,start,end,description,departure,arrival)
        }
    }
    localStorage.setItem('rides',JSON.stringify(rides));
}



function loadDays(dias){
    let checkboxs =document.getElementsByClassName('form-check-input');
    for(let i=0; i < checkboxs.length;i++){
        for(let j = 0; j < dias.length;j++){
            let d = dias[j];
            if(checkboxs[i].value == d){
                checkboxs[i].checked= true  
                console.log("funioomds")  
                console.log(d);
                console.log(dias.length);


            }else{
                console.log(dias.length);
                console.log(dias[0]);
                console.log(dias[1]);

            }
             
        }   
      
    }
}





function deleteRide(id){
    let rides = getRides();
    for(let i = 0; i < rides.length; i++){
        if(rides[i].Id === id){
            rides.splice(i,1);
        }
    }
    localStorage.setItem('rides',JSON.stringify(rides));
    location.reload();

}




